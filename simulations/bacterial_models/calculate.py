#!/usr/bin/env python
"""
Contains a few calculations for the figuring
out the equilibrium population density of a single
bacterial population that is propagated over multiple cycles.
"""
import numpy
from numpy import exp, nan, vectorize
from models import logistic_growth
#from scipy.optimize import brentq, minimize
from scipy.optimize import fmin

@vectorize
def lbound(func, x, cut):
    if x < cut:
        return func(cut) + abs(x-cut)
    else:
        return func(x)
@vectorize
def ubound(func, x, cut):
    if x > cut:
        return func(cut) + abs(x-cut)
    else:
        return func(x)

def validate_output(func):
    def wrapper(*args, **kwargs):
        DF = func(*args, **kwargs)
        DF[DF <= 1] = nan
        return DF

    return wrapper

def calculate_equilibrium_DF_model_01(Ai, At, Nsat, Ni, Nmin, gr, gd, Vmax, cycle_duration, **kwargs):
    """
    Returns
    ----------
    Tuple: (equilibrium dilution factor, equilibrium final density)
    """
    tf = cycle_duration
    tb = (Ai - At) / (Vmax * Ni)
    tb[tb < 0] = 0.0

    Ni_logistic = Ni * exp(-gd * tb)

    Ni_logistic[Ni_logistic < Nmin] = 0.0

    Nf = logistic_growth(r=gr, K=Nsat, x0=Ni_logistic, t=(tf-tb))
    Nf[Nf <= Ni] = nan
    DF = Nf/Ni
    return DF, Nf

def calculate_equilibrium_DF_model_02(Ai, At, Nsat, Ni, Nmin, gr, gd, tlag, Vmax, cycle_duration, **kwargs):
    """
    Calculates equilibrium dilution factor for a model with lag time.

    Returns
    ----------
    Tuple: (equilibrium dilution factor, equilibrium final density)
    """
    tf = cycle_duration
    tb = (Ai - At) / (Vmax * Ni)
    tb[tb < 0] = 0.0

    # Compute initial time at which logistic growth begins
    ti_logistic = numpy.array(tb)
    ti_logistic[tlag > tb] = tlag

    # Compute initial density for logistic growth
    x = tb - tlag
    x[x<0] = 0.0
    Ni_logistic = numpy.array(Ni * exp(-gd * x))
    Ni_logistic[Ni_logistic < Nmin] = 0.0

    Nf = logistic_growth(r=gr, K=Nsat, x0=Ni_logistic, t=(tf-ti_logistic))
    Nf[Nf <= Ni] = nan
    DF = Nf/Ni
    return DF, Nf

def calculate_bifurcation_DF(start, end, points=1000, **par_dict):
    """ Finds the dilution factor at which the bifurcation occurs. Not completely robust at the moment. """
    Ni = numpy.logspace(start, end, points)
    if 'tlag' in par_dict:
        return numpy.nanmax(calculate_equilibrium_DF_model_02(Ni=Ni, **par_dict))
    else:
        return numpy.nanmax(calculate_equilibrium_DF_model_01(Ni=Ni, **par_dict))

def vs_ni(**kwargs):
    def modified_func(Ni):
        z = calculate_equilibrium_DF_model_02(Ni=Ni, **kwargs)[1]
        #z[numpy.isnan(z)] = 

        return -z

    return func

def calculate_bifurcation_point_DF_model_02(**kwargs):
    # Need to find zero in this function
    #return fmin(lambda Ni : -(calculate_equilibrium_DF_model_02(Ni=Ni, **kwargs)[1]), 0.1)
    return fmin(vs_ni(**kwargs), 10.0, [1.0, 1000.0])
    #return minimize(vs_ni(**kwargs), 0.03)


if __name__ == '__main__':
    par_dict = {'gr': 1.0, 'Nsat': 1.0, 'gd': 3.0, 'Ai': 6, 'cycle_duration': 6.0, 'At': 2.0, 'Vmax': 200.0, 'tlag' : 0.0}
    Ni = numpy.logspace(-4, 0.0, 100)
    #Ni = numpy.array([0.01])

    #print vs_ni(Ni, **par_dict)

    #calculate_equilibrium_DF_model_02
    z = calculate_bifurcation_point_DF_model_02(**par_dict)
    print z
    #print calculate_equilibrium_DF_model_02(Ni=z, **par_dict)
