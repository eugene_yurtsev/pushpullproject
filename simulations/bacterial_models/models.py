#!/usr/bin/env python
import numpy
from numpy import linspace
import pylab
import time
import scipy.integrate

def logistic_growth(r, K, x0, t):
    """
    Solution of dx/dt = r x * (1-x)

    Given parameters: r, K
    Initial condition x0

    Evaluates the trajectory at time t
    """
    numerator = K * x0 * numpy.exp(r * t)
    denomenator = K + x0 * (numpy.exp(r * t) - 1.0)
    return numerator / denomenator

@numpy.vectorize
def death_function(Ni, Nmin, gr, t):
    Nf = Ni * numpy.exp(-gr * t)

    if Nf < Nmin:
        Nf = 0.0

    return Nf

#@numpy.vectorize
def model_1_analytic_nv(**pars):
    r"""
    This model incorporates no features at all except for antibiotic
    breakdown and logistic growth.
    \frac{dN}{dt} =
        -gd N, A > At  \\
        gr * N (Nsat - N), A < At \\
    \frac{dA}{dt} = - V_{max} * N_i \\ <-- proportional to initial number of bacteria

    Parameters
    ---------------

    Ai : float
        initial antibiotic concentration
    At : float
        Threshold antibitoic concentration
    Nsat : float
        Saturation density
    Ni : float
        Initial bacterial density
    gr : float
        growth rate
    gd : float
        death rate
    cycle_duration : float
        total duration of growth cycle
    t : array
        Specifies the time points at which to evaluate growth
    Returns
    ----------

    Cell density at each time point t
    """
    dA = pars['Ai'] - pars['At']
    Ni, Nsat = pars['Ni'], pars['Nsat']

    t = pars['t']

    if Ni > 0.0:
        tb = dA / (pars['Vmax'] * Ni)
    else:
        return 0.0

    if tb < 0:
        tb = 0.0

    gr, gd = pars['gr'], pars['gd']

    cycle_duration = pars['cycle_duration']
    Nmin = pars['Nmin']

    if numpy.any(t > cycle_duration):
        raise Exception('t cannot be larger than the cycle duration')

    if t < tb:
        return death_function(Ni, Nmin, gd, t)
    else:
        Ni_logistic = death_function(Ni, Nmin, gd, tb)
        return logistic_growth(gr, Nsat, Ni_logistic, (t-tb))

model_1_analytic = numpy.vectorize(model_1_analytic_nv) # Must keep for pickling to work properly

def model_2_analytic_nv(**pars):
    """
    Analytically solves bacterial growth model that incorporates:
        * Logistic Growth
        * Lag time
        * Antibiotic inactivation = dA/dt = - Vmax * Ni
    """
    Ai = pars['Ai']
    Ni = pars['Ni']

    t = pars['t']

    tlag = pars['tlag']

    if t < tlag:
        return Ni
    else:
        pars['cycle_duration'] -= tlag
        pars['t'] -= tlag
        pars['Ai'] -= Ni * pars['Vmax'] * tlag

        return model_1_analytic(**pars)

model_2_analytic = numpy.vectorize(model_2_analytic_nv) # Must keep for pickling to work properly

def bacterial_growth_based_on_rate_equations(**pars):
    """
    Returns the trajectory of the bacterial population density.

    Parameters
    -------------
    pars

    """

    # y0 is a vector that contains the initial conditions of the system
    # this simulator needs to keep track of both the levels of antibiotic and the population density
    y0 = (pars['Ni'], pars['Ai'])
    t = pars['t']

    def growth(vec, t):
        N = vec[0]
        A = vec[1]

        # in this system, the anitbiotic decreases at a rate proportional to the initial
        # density of cells in the mixture
        dA = -pars['Vmax'] * pars['Ni']

        # "At" is the threshhold, under which the cell population dies exponentially
        # and above which the population grows logistically
        if(A < pars['At']):
            dN = pars['gr']*N*(1 - (N*1.0/pars['Nsat']))
        else:
            dN = -pars['gd']*N

        #just a catch, as pars['Nmin'] corresponds to actual death in the system, a small number
        if(N < pars['Nmin']):
            dN = 0

        return [dN, dA]

    vec = scipy.integrate.odeint(growth, y0, t)

    #returns the last entry in vec, which has been integrated, N-final
    return vec[:, 0]

def generate_rate_equations_for_bacterial_growth(**pars):
    """
    Returns the trajectory of the bacterial population density.

    Parameters
    -------------
    Returns population density vs. time.

    Incorporates:
        * Lag time (no growth before t > tlag) : tlag
        * Switch MM inactivation on/off : MM_inactivation=True/False
        * Inactivation proportional to N(t) vs. N(i) : Ninit_inactivation = False vs. True
    """
    # y0 is a vector that contains the initial conditions of the system
    # this simulator needs to keep track of both the levels of antibiotic and the population density
    y0 = (pars['Ni'], pars['Ai'])
    t = pars['t']

    def growth(vec, t):
        N = vec[0]
        A = vec[1]

        # in this system, the anitbiotic decreases at a rate proportional to the initial
        # density of cells in the mixture

        if pars['Ninit_inactivation']:
            dA = -pars['Vmax'] * pars['Ni']
        else:
            dA = -pars['Vmax'] * N

        if pars['MM_inactivation']:
            dA *= A / (A + pars['Km'])

        # "At" is the threshhold, under which the cell population dies exponentially
        # and above which the population grows logistically
        if A < pars['At'] and t > pars['tlag']:
            dN = pars['gr']*N*(1 - (N*1.0/pars['Nsat']))
        else:
            dN = -pars['gd']*N

        #just a catch, as pars['Nmin'] corresponds to actual death in the system, a small number
        if(N < pars['Nmin']):
            dN = 0

        return [dN, dA]

    vec = scipy.integrate.odeint(growth, y0, t)

    #returns the last entry in vec, which has been integrated, N-final
    return vec[:, 0]

def generate_rate_equations_logistic_michaelis_inactivation_with_N_current_lag_time_release_upon_cell_death(**pars):
    """
    Returns population density vs. time.
    Incorporates:
        * Lag time (no growth before t > tlag)
        * Michaelis Menten antibiotic inactivation
        * Inactivation proportional to current number of cells
    """
    # y0 is a vector that contains the initial conditions of the system
    # this simulator needs to keep track of both the levels of antibiotic and the population density
    y0 = (pars['Ni'], pars['Ai'])
    t = pars['t']

    def growth(vec, t):
        N = vec[0]
        A = vec[1]

        # in this system, the anitbiotic decreases at a rate proportional to the initial
        # density of cells in the mixture
        Vmax = pars['Vmax']

        Ndead = pars['Ni'] - N
        Ndead = 0 if Ndead < 0 else Ndead

        Vmax = Vmax + Ndead * 10**7

        dA = -Vmax * N * A / (A + pars['Km'])

        # "At" is the threshhold, under which the cell population dies exponentially
        # and above which the population grows logistically
        if A < pars['At'] and t > pars['tlag']:
            dN = pars['gr']*N*(1 - (N*1.0/pars['Nsat']))
        else:
            dN = -pars['gd']*N

        #just a catch, as pars['Nmin'] corresponds to actual death in the system, a small number
        if(N < pars['Nmin']):
            dN = 0

        return [dN, dA]

    vec = scipy.integrate.odeint(growth, y0, t)

    #returns the last entry in vec, which has been integrated, N-final
    return vec[:, 0]

def generate_rate_equations_logistic_Ninit_inactivation_lag_time_min_resistant(**pars):
    """
    Returns population density vs. time.
    Incorporates:
        * Lag time (no growth before t > tlag)
        * Michaelis Menten antibiotic inactivation
        * Inactivation proportional to current number of cells
    """
    # y0 is a vector that contains the initial conditions of the system
    # this simulator needs to keep track of both the levels of antibiotic and the population density
    y0 = (pars['Ni'], pars['Ai'])
    t = pars['t']

    def growth(vec, t):
        N = vec[0]
        A = vec[1]

        # in this system, the anitbiotic decreases at a rate proportional to the initial
        # density of cells in the mixture
        Vmax = pars['Vmax']

        dA = -Vmax * pars['Ni']

        # "At" is the threshhold, under which the cell population dies exponentially
        # and above which the population grows logistically
        if A < pars['At'] and t > pars['tlag']:
            dN = pars['gr']*N*(1 - (N*1.0/pars['Nsat']))
        else:
            dN = -pars['gd']*N

        #just a catch, as pars['Nmin'] corresponds to actual death in the system, a small number
        if (N < pars['Nmin']):
            dN = 0

        #if N < pars['Nmin_res'] and dN < 0:
            #dN = 0
        if N < pars['Ni'] / 100.0 and dN < 0:
            dN = 0

        return [dN, dA]

    vec = scipy.integrate.odeint(growth, y0, t)

    #returns the last entry in vec, which has been integrated, N-final
    return vec[:, 0]

if __name__ == '__main__':
    pars = dict(Ni=0.01, Ai=0.0, At=0.3, gr=1.0, gd=3.0, tlag=3.0,
            cycle_duration=40.0, Vmax=100.0, Nmin=10**-6, Nsat=1.0)

    #models_list = model_1_analytic, bacterial_growth_based_on_rate_equations
    models_list = model_1_analytic, model_1_analytic_vectorized

    pars['t'] = [0, 40.0]

    for model in models_list:
        tic = time.time()
        m  = model(**pars)
        toc = time.time()
        print 'model {}. Time {}'.format(model, toc-tic)

