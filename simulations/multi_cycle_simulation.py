#!/usr/bin/env python
"""
Contains a few methods for cycling the simulation
for multiple days
"""
import numpy
from GoreUtilities.simulation import BaseSimulator
from PushPullProject.analysis import fitting

def get_par_dict():
    par_dict = {
            #The growth rate of the cells when the levels of ampicilin are under the threshhold
            'gr': 1.0, #cells/hour
            #The death rate of the cells when the levels of ampicilin are above the threshhold
            'gd' : 3.0, #cells/hour
            #The maximum density of the cells
            'Nsat' : 1.00, #OD
            #The minimum density of the cells (meaning complete death)
            'Nmin' : 10**-6, #OD
            #The velocity at which the cells 
            'Vmax' : 10, #(mg/mL)/(OD*hr)
            #The antibiotic threshhold under which the cells start dying
            'At' : 1.0, #mg/mL
            #The initial concentration of antibiotic the bacteria are exposed to
            'Ai' : 2.0, #mg/mL
            #The number of cycles that the stationary system undergoes
            'cycles' : 10,
            #The time that the cells are left to grow in between dilutions
            'cycle_duration' : 8, # hours
            'time_resolution' : 500, # density of points along time channel
            #The fraction of cells that travel from a given population upon migration
            'm' : 0.25
        }
    return par_dict

def simulate_single_population(**kwargs):
    """
    Propagates a single bacterial population over multiple cycles.
    """
    Npop = numpy.zeros(kwargs['cycles'])
    Npop[0] = numpy.array(kwargs['N1_single'])

    for i in range(1, kwargs['cycles']):
        Ni = Npop[i-1] / kwargs['dilution_factor']
        Npop[i] = simulate_bacterial_growth(Ni=Ni, **kwargs)[-1]

    return Npop

def simulate_bacterial_growth(**kwargs):
    """
    Calls the proper model to use for propagating through the cycle.
    """
    model = kwargs.pop('model')

    if 't' not in kwargs:
        kwargs['t'] = [kwargs['cycle_duration']]

    if 'N1_connected' in kwargs.keys(): # needed because using numpy.vectorize downstream
        kwargs.pop('N1_connected')
    return model(**kwargs)

def migrate(Ni, migration_rate):
    """
    Migrates the population using reflecting boundary conditions.

    Nn+1 = Nn (1-m) + (Nn_left + Nn_right) * m / 2
    At the boundary the population that was supposed to migrate across the boundary instead remains in place.

    Parameters
    ------------

    Ni : 1d array
        Describes the population sizes at each position in space
        NOTE: Ni must be at least length 2

    migration_rate : float
        Migration is carried according to the formula: Nn+1 = Nn (1-m) + (Nn_left + Nn_right) * m / 2

    Returns
    --------
    Nnew - migrated population
    """
    # Count connected poopulations
    gSize = len(Ni)

    Nleft  = Ni[numpy.arange(-1, gSize - 1) % gSize]
    Nright = Ni[numpy.arange(1,  gSize + 1) % gSize]

    # Migrate
    Nnew = Ni * (1-migration_rate) + (Nleft+Nright) * migration_rate / 2.0 # migrate population

    # Non-periodic boundary conditions (comment out to get periodic boundary conditions
    Nnew[0]  = (1-migration_rate/2.0) * Ni[0] + migration_rate / 2.0 * Ni[1]
    Nnew[-1] = (1-migration_rate/2.0) * Ni[-1] + migration_rate / 2.0 * Ni[-2]
    return Nnew

def simulate_connected_populations(N1_connected, **kwargs):
    """
    Code that simulates migrating populations.
    Ni = a 1d array encoding the initial population density of each population.
    dilution_factor = a scalar that indicates which dilution_factor to apply
    cycle_duration = time to integrate the differential equations for
    num_cycles = number of cycles to carry simulation for
    """
    num_cycles = kwargs['cycles']
    num_patches = len(N1_connected)
    Npop = numpy.zeros((num_cycles, num_patches)) # Preallocate memory for the simulation
    Npop[0, :] = numpy.array(N1_connected) # Initialize the first row

    #vecRunSim = get_run_sim_function(model_configuration)

    # Run simulation for num cycles
    for i in range(1, num_cycles):
        # Migrate
        Npop[i, :] = migrate(Npop[i-1, :], kwargs['m'])

        # Dilute
        Npop[i, :] = Npop[i, :] / kwargs['dilution_factor']

        # Run Growth cycle
        #Npop[i, :] = vecRunSim(Npop[i, :], Ai, cycle_duration)
        for c in range(num_patches): # TODO: vectorize for speed
            Ni = Npop[i, c]
            Npop[i, c] = simulate_bacterial_growth(Ni=Ni, **kwargs)[-1]
    return Npop

def simulation_function(**par_dict):
    single = simulate_single_population(**par_dict)[-1]
    connected = simulate_connected_populations(**par_dict)
    return single, connected

