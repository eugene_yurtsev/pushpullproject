#!/usr/bin/env python
"""
A set of routines that can fit a wave profile.

TODO: Need to move the part of the wave fitting from extractor to here.
"""
from scipy import optimize
import numpy
import GoreUtilities
from pylab import *

def fit_function(a, x_c, s, x):
    return a * (tanh((x_c - x)/s) + 1)

def residuals(pars, x, data):
    return data - fit_function(pars[0], pars[1], pars[2], x)

def fit_profile(data, a=1.0, x_c=1.0, s=1.0, amin=0.01):
    """
    Fits a single time point of a wave to a tanh function.

    Fits tanh function (y = a * tanh((x_c - x)/s) + h)

    Parameters
    -----------
    data : 1d array
        Describing the amplitude of the wave as a function of position
    a : float
        guess for the amplitude of the wave
    x_c : float
        guess for the center of the wave
    s : float
        guess for the width of the wave front
    amin : float
        Fudge parameter for minimal amplitude which is considered
        a successful fit. If a < amin, then nan is returned for each fitting parameter.
    Returns
    ----------
    fitting parameters [a, x_c, s, h]

    Returns [nan, nan, nan, nan] if something about the fit fails.
    """
    num_patches = len(data)
    x = arange(num_patches)

    A = optimize.leastsq(residuals , (a, x_c, s), args=(x, data), full_output=True)

    a, x_c, s = A[0]

    y0 = fit_function(*A[0], x=1)
    y1 = fit_function(*A[0], x=num_patches-1)

    #if abs(y1 - y0) < 0.01:
        #return [[numpy.nan, numpy.nan, numpy.nan]]

    if a < amin: # If the amplitude of the wave is small assume fit is unsuccessful
        return [[numpy.nan, numpy.nan, numpy.nan]]
    elif x_c > num_patches - 1 or x_c < 1:
        return [[numpy.nan, numpy.nan, numpy.nan]]

    return A

def fit_wave_profile_to_tanh(data, ignore_first=0, a=1.0, x_c=1.0, s=1.0, amin=0.01, **kwargs):
    """
    Fits a tanh to every time point. Then extracts wave parameters like velocity.

    The tanh function is (y = a * tanh((x_c - x)/s) + h)

    Parameters
    -----------
    data : 2d array
        Describing the amplitude of the wave as a function of position along the x axis.
        Time is along the y-axis.
    ignore_first : int
        If ignore_first = n, then ignores the first n time points.
    a : float
        guess for the amplitude of the wave
    x_c : float
        guess for the center of the wave
    s : float
        guess for the width of the wave front
    amin : float
        Fudge parameter for minimal amplitude which is considered
        a successful fit. If a < amin, then nan is returned for each fitting parameter.
    kwargs : dict
        Other parameters to be passed to the fitting function.

    Returns
    ----------

    fit_pars, velocity, velocity_fit_pars

    fit_pars : list
        each element corresponds to the fit parameters obtained for the wave profile
        at that time index

    velocity : float
        the velocity of the wave (position and time units normalized to 1)

    velocity_fit_pars : tuple
        slope and intercept
    """
    guesses = (a, x_c, s)
    kwargs['amin'] = amin

    fit_pars = [fit_profile(data[row, :], *guesses, **kwargs) for row in range(data.shape[0])]

    x_c = map(lambda x : x[0][1], fit_pars)
    #wave_amplitude = map(lambda x : 2*x[0][0], fit_pars)
    time = arange(data.shape[0])
    x_c = x_c[ignore_first:]
    time = time[ignore_first:]

    # filter out all nans
    valid_data = filter(lambda x: ~numpy.isnan(x[1]), zip(time, x_c))

    if len(valid_data) > 1:
        time, x_c = zip(*valid_data)
        velocity_fit_pars = numpy.polyfit(time, x_c, deg=1)
        velocity = velocity_fit_pars[0]
    else:
        velocity_fit_pars = numpy.nan, numpy.nan
        velocity = numpy.nan

    return fit_pars, velocity, velocity_fit_pars
