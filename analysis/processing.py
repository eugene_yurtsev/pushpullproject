#!/usr/bin/env python
from numpy import nan, arange

def find_wave_pin_indexes(velocities, zero_threshold):
    """
    Finds the start and end indexes when the wave is pinned (velocity is below a certain threshold).

    Parameters
    -----------
    velocities : ndarray
        list of velocities... assumed to be monotonic
    zero_threshold : float
        if absolute value of velocity is below this threshold, then the wave is considered to be stalled.

    Returns
    -----------
    pin_start_index, pin_end_index (integers)
    """
    indexes = arange(len(velocities))[abs(velocities) < zero_threshold]
    pin_start_index, pin_end_index = nan, nan

    if len(indexes) > 0:
        pin_start_index = indexes[0]
        pin_end_index = indexes[-1]

    return pin_start_index, pin_end_index
