#!/usr/bin/env python
"""
Contains extractor object whose role is to accept data (experimental or simulation)
and provide a common interface for analyzing that data.
Contains extractor objects that accept 2d matrices as input and
Module provides routines to fit 2d matrices that describe the propagation of a wave.
Position should be along the columns
Time is along the rows
"""
import numpy
import pandas
import pylab as plt
from pylab import *
from fitting import *
import processing

class WaveParExtractor(object):
    """
    Fits a 2d matrix that describes a traveling wave.
    Extracting wave parameters
    row -> time [normalized to units of 1]
    column -> column [normalized to units of 1]

    TODO: Should move the fitting of the wave top the fitting module.
    """
    def __init__(self, data, ignore_first=0, *guesses, **kwargs):
        """
        Parameters
        -----------
        data : ndarray (2d)
            describes the population densities of the meta population
            position along columns
            time along rows
        ignore_first : int
            If ignore_first = n, then ignores the first n time points.
        guesses : initial guesses for fits. Check fit_profile function for more documentation.
        kwargs : passed to the fitting routine as well
        """
        self.data = data
        self.ignore_first = ignore_first
        self.fit(*guesses, **kwargs)

    def fit(self, *guesses, **kwargs):
        """ Fits the wave and extracts the parameters. """
        results = fit_wave_profile_to_tanh(self.data, self.ignore_first, *guesses, **kwargs)
        self.fit_pars = results[0]
        self.velocity = results[1]
        self.velocity_fit_pars = results[2]

    @property
    def x_c(self):
        """ Returns the centers of the waves. """
        return numpy.array(map(lambda x : x[0][1], self.fit_pars))

    @property
    def wave_amplitude(self):
        """ Returns the centers of the waves. """
        return numpy.array(map(lambda x : 2*x[0][0], self.fit_pars))

    def get_fit_profile(self, time_index):
        """ Returns a sample fit profile using the fit parameters for the given time_index. """
        x = arange(0, self.data.shape[1], 0.5)
        return x, fit_function(*self.fit_pars[time_index][0], x=x)

    def plot_profiles(self, indexes=None, cmap=cm.copper, **kwargs):
        kwargs.setdefault('marker', 'o')
        kwargs.setdefault('linestyle', '--')
        color_array = cmap(linspace(0, 1, self.data.shape[0]))

        if indexes is None:
            indexes = range(self.data.shape[0])

        for index in indexes:
            plot(range(self.data.shape[1]), self.data[index, :], color=color_array[index], **kwargs)
            xfit, yfit = self.get_fit_profile(index)
            plot(xfit, yfit, '-k')

        xlabel('Well', size=16)
        ylabel('Density', size=16)

    def plot_heat_map(self, **kwargs):
        kwargs.setdefault('cmap', cm.Blues)
        kwargs.setdefault('include_values', False)
        out = GoreUtilities.plot_heat_map(self.data, **kwargs)
        xlabel('Well', size=16)
        ylabel('Cycle', size=16)
        return out

    def plot_velocity_fit(self, **kwargs):
        time = arange(len(self.x_c))
        index = self.ignore_first
        plot(time[index:], self.x_c[index:], marker='o', color='b', linestyle='none') # Part used for fit
        plot(time[:index], self.x_c[:index], marker='o', color='gray', linestyle='none') # part not used for fit
        x = arange(0, self.data.shape[0])
        y = polyval(self.velocity_fit_pars, x)
        plot(x, y, '-', color='k')
        text(0.1, 0.9, 'Velocity = {}'.format(self.velocity), transform=plt.gca().transAxes)
        xlabel('Time', size=16)
        ylabel('Position of Wave Center', size=16)

    def plot(self):
        """
        Plots the migrating wave together with the fits and the velocity
        """
        subplot(1, 3, 1)
        self.plot_heat_map()
        subplot(1, 3, 2)
        self.plot_profiles()
        subplot(1, 3, 3)
        self.plot_velocity_fit()


class VelocityDependenceAnalyzer(object):
    """
    This is an extractor/analyzer that accepts
    """
    def __init__(self, data):
        """
        Parameters
        ------------
        data : DataFrame
            columns : control_par, velocity
        """
        self.data = data

    @classmethod
    def from_wave_par_object_series(cls, wave_par_series, control_parameter_name=None):
        """
        Parameters
        -------------
        control_par_name : str
            Name of the control parameter
        wave_par_series : Series
            Make sure that the index.name property is set to the correct control parameter
        control_par_name : None | str
            If provided, sets the name of the index
        """
        data = pandas.DataFrame(wave_par_series, columns=['wave_obj'])
        data['velocity'] = data['wave_obj'].apply(lambda x : x.velocity)
        if control_parameter_name is not None:
            data.index.name = control_parameter_name
        return cls(data)

    def extract_pin_indexes(self, amplitude_threshold):
        self.pin_start_index, self.pin_end_index =\
                processing.find_wave_pin_indexes(self.data.velocity.values, amplitude_threshold)

    @property
    def pin_start_location(self):
        if self.pin_start_index is nan:
            return nan
        else:
            return self.data.index[self.pin_start_index]

    @property
    def pin_end_location(self):
        if self.pin_end_index is nan:
            return nan
        else:
            return self.data.index[self.pin_end_index]

    @property
    def pin_ratio(self):
        return self.pin_end_location/float(self.pin_start_location)

    def plot(self, annotate=True, **kwargs):
        ax = kwargs.get('ax', plt.gca())
        out = self.data.velocity.plot(**kwargs)
        if annotate:
            ax.set_ylabel('Velocity (Wells / Cycle)')
        return out

    def plot_pin_locations(self, **kwargs):
        """
        Plots the locations where velocity pinning starts and ends
        """
        kwargs.setdefault('color', 'k')
        if self.pin_start_location != self.pin_end_location:
            axvline(self.pin_start_location, label='pin start', linestyle='-', **kwargs)
            axvline(self.pin_end_location, label='pin end', linestyle='--', **kwargs)
        else:
            axvline(self.pin_start_location, label='pin start/end', linestyle='--', **kwargs)
        axhline(0, linestyle='-', color='gray')

    def plot_pinned_profiles(self, index_range, annotate=True, **kwargs):
        """
        Plots the profiles of the pinned waves.

        Parameters
        --------------
        time_range : [int]
            list of time indexes at which to plot the wave
        annotate : True | False
            If true, annotates the plot
        """
        if isnan(self.pin_start_index) or isnan(self.pin_end_index): return

        index_list = range(self.pin_start_index, self.pin_end_index+1) # +1 because the end is not included

        for i, index in enumerate(index_list):
            plot(self[index].plot_profiles(index_range))

        if annotate:
            xlabel('Well')
            ylabel('Population Density')

    def __getitem__(self, key):
        """ Get individual fits. """
        return self.data['wave_obj'].iloc[key]


class MessyExtractor(object):
    """
    This is a messy extractor that requires refactoring
    so that the single population logic is separate
    from the connected population logic.
    """
    def gen_wave_data_sequence(self):
        for loc, wave_data in numpy.ndenumerate(self.results):
            yield loc, wave_data[1]

    def _annotate_bifurcation(self, ax=None):
        """
        Annotates the bifurcation diagram
        """
        if ax is None:
            ax = plt.gca()

        name = self.loop_pars[0]
        name = name.replace('_', ' ').title()

        ax.set_ylabel('Population Density (OD)', size=16.0)
        ax.set_xlabel(name, size=16.0)
        ax.set_yscale('symlog', linthreshy=10**-6)
        ax.set_xscale('log')
        ax.set_ylim(0, 1.1)

    def extract_bifurcation_pars(self):
        dict_copy = dict(self.par_dict)

        if 'Ni' in dict_copy.keys():
            dict_copy.pop('Ni')

        if self.loop_pars[0] == 'dilution_factor':
            self.bifurcation_point = calculate.calculate_bifurcation_DF(-4, 1, 1000, **dict_copy)
        else:
            single_population_densities = numpy.array(self.single_population_densities)
            index = numpy.where(single_population_densities < 0.01)[0][0]
            self.bifurcation_point = nan
            if index > 0:
                val = (self.loop_par_values[0][index] + self.loop_par_values[0][index-1])/2.0
                self.bifurcation_point = val

    def plot_single_pop_density(self, ax=None, annotate=False, **kwargs):
        """
        Plots the final population density of the single population vs. the control parameter
        """
        if ax is None:
            ax = plt.gca()

        x = array(self.loop_par_values).flatten()
        hp = ax.plot(x, self.single_population_densities, **kwargs)
        if annotate:
            self._annotate_bifurcation(ax)
        return hp

    def plot_bifurcation_analytical(self, annotate=False, Ni=numpy.logspace(-4, 1, 1000), **kwargs):
        """
        TODO: Refactor need to call a method of the model itself
        """
        if self.loop_pars[0] <> 'dilution_factor':
            raise Exception('analytical calculation only availabel for control parameter = dilution factor')
        dict_copy = dict(self.par_dict)

        if 'Ni' in dict_copy.keys():
            dict_copy.pop('Ni')

        dict_copy.setdefault('tlag', 0.0)

        DF, Nf = calculate.calculate_equilibrium_DF_model_02(Ni=Ni, **dict_copy)
        hp = plot(DF, Nf, **kwargs)
        axvline(self.bifurcation_point, color='k') # Equilibrium dilution factor
        if annotate:
            self._annotate_bifurcation()
        return hp

