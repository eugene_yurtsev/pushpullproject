#!/usr/bin/env python
"""
Helps to deal with meta data.

Useful functions
--------------------
    get_meta
        Fetches a .csv file that is located online and returns a DataFrame
    get_forward_PID_list
        given the starting index (PID) and a DataFrame (meta) gives the sequences of PIDs that follow.
        (Does not check for infinite loops)
"""

import requests
import numpy
from pandas import DataFrame

def get_meta(URL='https://docs.google.com/spreadsheet/pub?key=0AhEhy-QI6cqsdEdRaUZWbHdrb0h0X1ZiaENnN196Rnc&output=csv'):
    """
    Reads the excel spread sheet at the given URL and uses it to create a pandas DF.
    """
    response = requests.get(URL)
    content = response.content
    with open('temp.csv', 'w') as f:
        f.write(content)
    return DataFrame.from_csv('temp.csv', index_col=0)

def get_forward_ID_list(start_ID, meta, nextIDfield='nextID'):
    """
    Parameters
    ----------
    start_ID : int | float | str
        The starting ID (used as the index in the DataFrame)

    meta : DataFrame
        Contains the meta data information.

    nextIDfield : str
        column that maps the current row to the next row (ID)

    Returns
    ---------

    list of IDs
    """
    current_ID = start_ID
    ID_list = [start_ID]
    while current_ID > 0:
        current_ID = float(meta.loc[current_ID][nextIDfield])
        if current_ID is not numpy.nan:
            ID_list.append(current_ID)
    return ID_list

if __name__ == '__main__':
    print get_meta()
